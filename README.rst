VTK Visualizer Control
======================

Easy visualization of point clouds and geometric primitives based on VTK

See http://www.edge.no/wiki/Python_VTK for more information